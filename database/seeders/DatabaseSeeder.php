<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(20)->create();
        \App\Models\Country::factory(10)->create();
        \App\Models\City::factory(10)->create();
        \App\Models\Place::factory(10)->create();
        $person = \App\Models\Person::factory(10)->create()
        /*->each(function ($person) use ($job){
            $person->job()->attach(
                $job->random(rand(1, 3))->pluck('id')->toArray()
            );
        })*/;
        $job = \App\Models\Job::factory(10)->create()
        ->each(function ($job) use ($person){
            $job->person()->attach(
                $person->random(rand(0, 2))->pluck('id')->toArray()
            );
        });
        \App\Models\TypeOfContainer::factory(10)->create();
        \App\Models\Container::factory(10)->create();
        \App\Models\Route::factory(10)->create();
        \App\Models\Cargo::factory(10)->create();
        \App\Models\Seal::factory(10)->create();
        \App\Models\Port::factory(10)->create();

        \App\Models\User::firstOrCreate(['name' => 'Teste','email' => 'teste@teste.com'], ['password' => bcrypt('12345678'), 'updated_at' => date('Y-m-d H:i:s'), ]);
    }
}
