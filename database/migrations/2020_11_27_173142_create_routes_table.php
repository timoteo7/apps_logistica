<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('customerreference', 190);
            $table->dateTime('receipttime')->nullable();
            $table->unsignedBigInteger('expedidor_id')->nullable();
            $table->foreign('expedidor_id')->references('id')->on('persons');
            $table->unsignedBigInteger('consignee_id')->nullable();
            $table->foreign('consignee_id')->references('id')->on('persons');
            $table->unsignedBigInteger('notify_id')->nullable();
            $table->foreign('notify_id')->references('id')->on('persons');
            $table->unsignedBigInteger('placereceipt_id')->nullable();
            $table->foreign('placereceipt_id')->references('id')->on('places');
            $table->unsignedBigInteger('placedelivery_id')->nullable();
            $table->foreign('placedelivery_id')->references('id')->on('places');
            $table->string('status', 50)->nullable();
            $table->string('situation', 50)->nullable ();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('routes');
    }
}
