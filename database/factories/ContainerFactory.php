<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Container;

class ContainerFactory extends Factory
{
    /**
    * The name of the factory's corresponding model.
    *
    * @var  string
    */
    protected $model = Container::class;

    /**
    * Define the model's default state.
    *
    * @return  array
    */
    public function definition()
    {
        return [
            'code' => $this->faker->iban($countryCode = null ),
            'person_id' => function () {
                $parent_model='\App\Models\Person';
                return ( is_null( $parent_model::first()) || ($this->faker->boolean($chanceOfGettingTrue = 10))  )
                    ? $parent_model::factory()
                    : $parent_model::inRandomOrder()->first()->id;
            },
            'typeofcontainer_id' => function () {
                $parent_model='\App\Models\TypeOfContainer';
                return ( is_null( $parent_model::first()) || ($this->faker->boolean($chanceOfGettingTrue = 10))  )
                    ? $parent_model::factory()
                    : $parent_model::inRandomOrder()->first()->id;
            },
            'obs' => $this->faker->text,
        ];
    }
}
