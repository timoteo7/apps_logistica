<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\City;

class CityFactory extends Factory
{
    /**
    * The name of the factory's corresponding model.
    *
    * @var  string
    */
    protected $model = City::class;

    /**
    * Define the model's default state.
    *
    * @return  array
    */
    public function definition()
    {
        return [
            'name' => $this->faker->city,
            'state' => $this->faker->state,
            'country_id' => function () {
                $parent_model='\App\Models\Country';
                return ( is_null( $parent_model::first()) || ($this->faker->boolean($chanceOfGettingTrue = 10))  )
                    ? $parent_model::factory()
                    : $parent_model::inRandomOrder()->first()->id;
            },
        ];
    }
}
