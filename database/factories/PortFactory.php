<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Port;

class PortFactory extends Factory
{
    /**
    * The name of the factory's corresponding model.
    *
    * @var  string
    */
    protected $model = Port::class;

    /**
    * Define the model's default state.
    *
    * @return  array
    */
    public function definition()
    {
        return [
            'isport' => $this->faker->boolean,
            'name' => $this->faker->name,
            'abbreviation' => $this->faker->unique()->countryCode . strtoupper($this->faker->unique()->randomLetter),
            'address' => $this->faker->address,
            'city_id' => \App\Models\Person::inRandomOrder()->first()->id,
            'phone' => $this->faker->phoneNumber,
            'obs' => $this->faker->text,
        ];
    }
}
