<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Cargo;

class CargoFactory extends Factory
{
    /**
    * The name of the factory's corresponding model.
    *
    * @var  string
    */
    protected $model = Cargo::class;

    /**
    * Define the model's default state.
    *
    * @return  array
    */
    public function definition()
    {
        return [
            'route_id' => \App\Models\Route::inRandomOrder()->first()->id,
            'container_id' => \App\Models\Container::inRandomOrder()->first()->id,
            'packages' => $this->faker->numberBetween($nbMaxDecimals = 2, $min = 1, $max = 999),
            'weight' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 30, $max = 200),
            'volume' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 100),
            'amount' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 500, $max = 999),
        ];
    }
}
