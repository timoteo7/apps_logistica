<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\TypeOfContainer;

class TypeOfContainerFactory extends Factory
{
    /**
    * The name of the factory's corresponding model.
    *
    * @var  string
    */
    protected $model = TypeOfContainer::class;

    /**
    * Define the model's default state.
    *
    * @return  array
    */
    public function definition()
    {
        return [
            'description' => $this->faker->catchPhrase,
            'length' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 50 )*3,
            'width' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 100 )*15,
            'height' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 20 )*5,
            'tare' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 100 ),
        ];
    }
}
