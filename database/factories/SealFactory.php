<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Seal;

class SealFactory extends Factory
{
    /**
    * The name of the factory's corresponding model.
    *
    * @var  string
    */
    protected $model = Seal::class;

    /**
    * Define the model's default state.
    *
    * @return  array
    */
    public function definition()
    {
        return [
            'cargo_id' => \App\Models\Cargo::inRandomOrder()->first()->id,
            'seal' => $this->faker->iban($countryCode = null ),
        ];
    }
}
