<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Place;

class PlaceFactory extends Factory
{
    /**
    * The name of the factory's corresponding model.
    *
    * @var  string
    */
    protected $model = Place::class;

    /**
    * Define the model's default state.
    *
    * @return  array
    */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'city_id' => function () {
                $parent_model='\App\Models\City';
                return ( is_null( $parent_model::first()) || ($this->faker->boolean($chanceOfGettingTrue = 10))  )
                    ? $parent_model::factory()
                    : $parent_model::inRandomOrder()->first()->id;
            },
        ];
    }
}
