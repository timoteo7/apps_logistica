<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Route;

class RouteFactory extends Factory
{
    /**
    * The name of the factory's corresponding model.
    *
    * @var  string
    */
    protected $model = Route::class;

    /**
    * Define the model's default state.
    *
    * @return  array
    */
    public function definition()
    {
        return [
            'customerreference' => $this->faker->swiftBicNumber,
            'receipttime' => $this->faker->dateTime(),
            'expedidor_id' => \App\Models\Person::inRandomOrder()->first()->id,
            'consignee_id' => \App\Models\Person::inRandomOrder()->first()->id,
            'notify_id' => \App\Models\Person::inRandomOrder()->first()->id,
            'placereceipt_id' => \App\Models\Place::inRandomOrder()->first()->id,
            'placedelivery_id' => \App\Models\Place::inRandomOrder()->first()->id,
            'status' => $this->faker->word,
            'situation' => $this->faker->word,
        ];
    }
}
