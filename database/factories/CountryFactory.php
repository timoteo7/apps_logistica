<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Country;

class CountryFactory extends Factory
{
    /**
    * The name of the factory's corresponding model.
    *
    * @var  string
    */
    protected $model = Country::class;

    /**
    * Define the model's default state.
    *
    * @return  array
    */
    public function definition()
    {
        return [
            'name' => $this->faker->country,
            'abbreviation' => $this->faker->unique()->countryCode . strtoupper($this->faker->unique()->randomLetter),
            'iso' => $this->faker->unique()->numberBetween($min = 100, $max = 999),
            'flag_url' => $this->faker->imageUrl($width = 23, $height = 15),
        ];
    }
}
