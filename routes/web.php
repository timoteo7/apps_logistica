<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes([  'reset'     => true,
                'verify'    => false,
                'confirm'   => false,
]);

Route::get('/', function () {
    Auth::loginUsingId(1);
    return redirect('/user');
});


Route::get('/home', function() {
    return view('home',['path' => 'users', 'referencia'=> 'user', 'model'     => 'User', 'dataTable' => [] ]);
})->name('home')->middleware('auth');

$data = json_decode(File::get(base_path() ."/routes/web.json"));
foreach ($data as $key => $obj) {

    Route::get(
        '/'.$key, [
            'uses'      => 'App\Http\Controllers\CrudController@firstRender',
            'referencia'=> $obj->referencia,
            'path'      => $obj->path,
            'model'     => $obj->model,
        ]
    )->middleware('auth');

}
