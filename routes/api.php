<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

	Route::apiResources([
		'ports' 			=>'App\Http\Controllers\CrudController',
		'seals' 			=>'App\Http\Controllers\CrudController',
		'cargos' 			=>'App\Http\Controllers\CrudController',
		'routes' 			=>'App\Http\Controllers\CrudController',
		'containers' 		=>'App\Http\Controllers\CrudController',
		'typesofcontainer'	=>'App\Http\Controllers\CrudController',
		'jobs' 				=>'App\Http\Controllers\CrudController',
		'persons' 			=>'App\Http\Controllers\CrudController',
		'places' 			=>'App\Http\Controllers\CrudController',
		'cities' 			=>'App\Http\Controllers\CrudController',
		'users'  			=>'App\Http\Controllers\CrudController',
		'countries'  		=>'App\Http\Controllers\CrudController',
	]);
