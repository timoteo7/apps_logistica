@extends('adminlte::page')

@section('title', config('adminlte.title'))

@section('content_header')
    <h1 class="m-0 text-dark">Dashboard</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div id="app" class="card-body">
                    <!--crud referencia='user' ></crud-->
                    <div v-if="isLoading">
                      ABRINDO...
                    </div>
                    <crud v-else vue_options="{{json_encode(compact( 'path','referencia','dataTable' ))}}" ></crud>
                    <notifications group="avisos" position="bottom right" />
                </div>
            </div>
        </div>
    </div>
@stop
