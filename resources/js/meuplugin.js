
export default {
  install (Vue) {

    Vue.mounta_schema_json = (data) => {
      const that = this;
      const schema = [];

      data.forEach(function (item) {
        const objeto = { type: "input", inputType: "text" };
        
        if ((item.hasOwnProperty("inForm") && (item.inForm)))
        {
          var keys = Object.keys(item);
          keys.forEach(function(key) {
            if (key == 'name'){
              objeto[key] = objeto.model = item[key]
              if(!item.hasOwnProperty("label"))
                objeto.label = objeto.placeholder = item[key]
            }else
            if (key == 'htmlType'){
              if(item[key] != 'text' && item[key] != null )
                switch (item[key]) {
                  case "textarea" : objeto.type="textArea";break;
                  //case "datetime" : objeto.type="pikaday";break;
                  default:  objeto.inputType = item[key]
                }
            }else
            if ( ['inForm', 'inIndex', 'styleClasses', 'label' ].includes(key)  ) objeto[key] = item[key]
            else
            if ( ['dbType', 'validations', 'searchable', 'fillable', 'primary', 'inView'].includes(key)  ) null
            else
            objeto[key] = item[key]
          });

          schema.push(objeto)
        }
        else
        if (item.hasOwnProperty("inIndex") && (item.inIndex)){
          objeto.inIndex = true;
          objeto.inputType = "hidden"
          objeto.model = item.name
          schema.push(objeto)
        }

        /*
        if (item.inForm) {
          const objeto = { type: "input", inputType: "text" };

          if (item.hasOwnProperty("model")) objeto.label = objeto.model = item.model; //objeto.placeholder
          if (item.hasOwnProperty("label")) objeto.label = item.label; //objeto.placeholder
          if (item.hasOwnProperty("placeholder"))
            objeto.placeholder = item.placeholder;
          if (item.hasOwnProperty("styleClasses")) objeto.styleClasses = item.styleClasses;
          else objeto.styleClasses = "col-md-6";
          if (item.hasOwnProperty("required") && (item.required == 'true') ) {
            objeto.required = true;
            objeto.validator = 'string';
          }
          if (item.hasOwnProperty("disabled")) objeto.disabled = item.disabled;
          if (item.hasOwnProperty("default")) objeto.default = item.default;
          if (item.hasOwnProperty("buttonText")) objeto.buttonText = item.buttonText;
          if (item.hasOwnProperty("ref")) objeto.ref = item.ref;

          if (item.hasOwnProperty("onSubmit")) objeto.onSubmit = function (model, schema, event) { that[item['onSubmit']](model, schema, event) }

          if (item.hasOwnProperty("onChanged")) objeto.onChanged = function (model, newVal, oldVal, field) { that[item['onChanged']](model, newVal, oldVal, field) }

          if (item.hasOwnProperty("inputType")) {
            if ( (item.inputType == "select") || (item.inputType == "dateTimePicker") || (item.inputType == "vueMultiSelect") || (item.inputType == "textArea") || (item.inputType == "cleave") || (item.inputType == "radios")) {
              
              objeto.type = item.inputType;

              if (item.inputType == "dateTimePicker")objeto.inputType = "datetime";else
              if (item.inputType == "cleave"){
                objeto.cleaveOptions = {
                  numeral: true,
                  numeralThousandsGroupStyle: 'thousand',
                  numeralDecimalScale: 2,
                  numeralDecimalMark: ',',
                }
              } else
              if (item.inputType == "vueMultiSelect"){
                objeto.inputType = "text";
                objeto.selectOptions = {
                  //label: item.customLabel[0],
                  //key: item.customLabel[0],
                  //trackBy:item.customLabel[3],
                  selectLabel:" enter seleciona",
                  onSearch: function(searchQuery, id, options) {
                    if (searchQuery.length === 0) {
                      this.countries = []
                    } else {
                      this.optionsFiltered = this.options.filter(e => (e.cpf_cnpj !== null && e.cpf_cnpj.toLowerCase().includes(query.toLowerCase())) || ( e.nome !== null && e.nome.toLowerCase().includes(query.toLowerCase())) )
                    }
                  },
                  //internalSearch : true,
                  openDirection:'top',
                  //closeOnSelect: true,
                  //resetAfter: true,
                  //clearOnSelect:true, // !!! multiple
                  
                  //searchable: true,
                  customLabel: function( objeto ) {
                    return `${objeto[item.customLabel[0]]} — [${objeto[item.customLabel[1]]}]`
                  }
                  
                  //onUpdate:
                  //onNewTag:
                }

                if(item.hasOwnProperty("customLabel"))
                {
                  Object.assign(objeto.selectOptions, {
                    label: item.customLabel[0],
                    //key: item.customLabel[2],
                    trackBy:item.customLabel[2],
                  });
                  objeto.selectOptions.customLabel = function( objeto ) {
                    return `${objeto[item.customLabel[0]]} — [${objeto[item.customLabel[1]]}]`
                  }
                }
                  //:options="optionsFiltered"
                  //@search-change="search"
                  //:internal-search="false"
                  //track-by="id"
                  //label="nome"

              } else
                objeto.inputType = "text";

            } else 
            if (item.inputType == "submit") objeto.type = item.inputType; else
            objeto.inputType = item.inputType;
          }

          if (item.hasOwnProperty("values")) {
            if (item.inputType != "vueMultiSelect") objeto.selectOptions = { noneSelectedText: "< Nada Selecionado >" };
            if (Array.isArray(item.values)) objeto.values = item.values;
            else
              objeto.values = JSON.parse(
                localStorage.getItem(item.values.localStorage)
              );
          }

          if (item.hasOwnProperty("value")) {
            //objeto.value = objeto.values.find( _ => _.id === item.value )
            objeto.value = item.value
          }

          schema.push(objeto);
        }*/
      });
      //console.log(JSON.stringify(schema))
      return schema;
    },
    
    Vue.pluckIfIsTrue  = (data, key = 'model', isTrue = null) => {
      var array = []
      for(var i in data){
        if ( data[i].hasOwnProperty(key) && (isTrue === null || (data[i].hasOwnProperty(isTrue) && data[i][isTrue]) ) )
          array.push(data[i][key])
      }
      return array
    }

    Vue.pluck  = (data, key = 'model') => {
      var array = [];
      for(var i in data){
        if(data[i].hasOwnProperty(key))
          array.push(data[i][key])
      }
      return array;
    }

    Vue.converte  = (parameter = 'referencia', value = 'model') => {
      var array= []
      var routes = require("./../../routes/web.json");
      var parents = Vue.pluck(routes, parameter)
      var childs = Vue.pluck(routes, value)
      for(var i in parents) {array[parents[i]]=childs[i];}
      return array;
    }


    Vue.filter('formato', (date, hora=false) => { if (typeof(date) !== 'undefined') {
      options = {
        year  : 'numeric',
        month : '2-digit',
        day   : '2-digit',
      };
      if (hora) options = Object.assign({}, options, {
        hour  : '2-digit',
        minute: '2-digit',
      });
      return new Intl.DateTimeFormat('pt-BR', options).format(new Date(date))
    }})

    Vue.filter('monetario', (value) => (typeof value !== "number") ? value :
      new Intl.NumberFormat('pt-BR', { currency: 'BRL', style: 'currency', minimumFractionDigits: 2, }).format(value))

    Vue.toggle = (field) => { // sumir um campo gerado...
        this.schema.fields.forEach((schema) => {
          if(schema.model == field) {
            this.$set(schema, 'visible', schema.visible == undefined ? false : !schema.visible);
            //schema.visible = !schema.visible;
            console.log('toggled visible', schema.visible, this.schema.fields);
          }
        });
      },

    Vue.myGlobalMethod = function () {
      console.log('Method!');
      return 'ola';
    }

    // 2. adicionar um recurso global
    Vue.directive('my-directive', {
      bind (el, binding, vnode, oldVnode) {
        console.log('diretiva!');
        return 'direto';
      }
    })

    // 3. adicionar algumas opções de componente
    Vue.mixin({
      created: function () {
        //console.log('created!');
      }
    })

    // 4. adicionar um método de instância
    Vue.prototype.$_myMethod = function () {
      console.log('instancia!');
      return 'oi';
    }

    Vue.prototype.$surname = 'Smith'
  }
}
