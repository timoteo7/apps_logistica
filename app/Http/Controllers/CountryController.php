<?php

namespace App\Http\Controllers;

use App\Models\Country;
use Illuminate\Http\Request;
use Validator;

class CountryController extends Controller
{
    private static $model = "App\Models\Country";


    /**
     * Display a listing of the resource first time.
     *
     * @return \Illuminate\Http\Response
     */
    public function firstRender(Request $request)
    {
        $data=self::$model::get();
        return view(
            'home', [
                'dataTable' => $data,
                'referencia'=> $request->route()->action['referencia'],
                'path'   => $request->route()->action['path'],
            ]
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=self::$model::get();
        return response()->json($data, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['ip'] = request()->ip();
        if (is_array($validated = $this->validateRequest($request))) {
            $data=self::$model::create($validated);
            return response()->json($data, 201);
        }
        else return $validated;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function show(Country $country)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Country $country)
    {
        if (is_array($requisicao = $this->validateRequest($request, null )) ) {
            $country->update($requisicao);
            return response()->json($country, 200);
        }
        else return $requisicao;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function destroy(Country $country)
    {
        return response()->json($country->delete(), 204);
    }

    public function validateRequest(Request $request, $rules = null)
    {
        $data=$request->isJson() ? $request->json()->all() : $request->all();
        if(empty($data)) return response('Vazio', 204);
         
        if (property_exists(self::$model, 'rules'))
        {
            if(is_null($rules)) $rules=self::$model::$rules;
            $validator = Validator::make($data, $rules);
            if ($validator->fails()) { return  response()->json($validator->errors(), 422); }
            else return $data;
        }
        else
        return $data;
    }
}
