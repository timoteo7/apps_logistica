<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use App\Traits\CrudTrait;

class CrudController extends Controller
{

    private static $model = "";

    public function __construct(Request $request)
    {
        if(!is_null($request->route()))
        {
            if(array_key_exists('model',$request->route()->action))    // from web route to firstRender
            {
                //$req_action = @$request->route()->getAction();
                self::$model = "App\Models\\" . $request->route()->action['model']; //$request->route()->action['model'];
            }
            else    //Route from apiResources
            {
                $name=$request->route()->getName();
                if(strpos($name,'.')) 
                    $path=substr($name, 0, strpos($name,'.'));
                else
                    $path=$name;
        
                $data = json_decode(File::get(base_path() ."/routes/web.json"), true);
                foreach ($data as $array) {
                    if($array['path']==$path) {
                        self::$model = "App\Models\\" . $array['model'];break;
                    }
                }
            }
        }
    }
    
    use CrudTrait;

}
