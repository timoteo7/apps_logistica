<?php

namespace App\Http\Controllers;

//use App\Models\User;
//use Illuminate\Http\Request;
use App\Traits\CrudTrait;

class UserController extends Controller
{
    private static $model = "App\Models\User";
    use CrudTrait;


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

}
