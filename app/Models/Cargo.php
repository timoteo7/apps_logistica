<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Cargo
 * @package App\Models
 * @version December 1, 2020, 8:01 pm UTC
 *
 * @property \App\Models\Container $container
 * @property \App\Models\Route $route
 * @property integer $route_id
 * @property integer $container_id
 * @property integer $packages
 * @property number $weight
 * @property number $volume
 * @property number $amount
 */
class Cargo extends Model
{
	use \Illuminate\Database\Eloquent\Factories\HasFactory;

    public $table = 'cargos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $with = ['route', 'container'];



    public $fillable = [
        'route_id',
        'container_id',
        'packages',
        'weight',
        'volume',
        'amount'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'route_id' => 'integer',
        'container_id' => 'integer',
        'packages' => 'integer',
        'weight' => 'float',
        'volume' => 'float',
        'amount' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'route_id' => 'nullable',
        'container_id' => 'nullable',
        'packages' => 'nullable|integer',
        'weight' => 'nullable|numeric',
        'volume' => 'nullable|numeric',
        'amount' => 'nullable|numeric',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function container()
    {
        return $this->belongsTo(\App\Models\Container::class, 'container_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function route()
    {
        return $this->belongsTo(\App\Models\Route::class, 'route_id');
    }
}
