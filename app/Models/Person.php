<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Person
 * @package App\Models
 * @version November 26, 2020, 7:16 pm UTC
 *
 * @property \App\Models\City $city
 * @property string $name
 * @property string $address
 * @property string $phone
 * @property integer $city_id
 * @property string $obs
 */
class Person extends Model
{
	use HasFactory;

    public $table = 'persons';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $with = ['city', 'job'];


    public $fillable = [
        'name',
        'address',
        'phone',
        'city_id',
        'obs'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'address' => 'string',
        'phone' => 'string',
        'city_id' => 'integer',
        'obs' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string|max:190',
        'address' => 'nullable|string|max:190',
        'phone' => 'nullable|string|max:50',
        'city_id' => 'nullable',
        'obs' => 'nullable|string',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function city()
    {
        return $this->belongsTo(\App\Models\City::class, 'city_id');
    }

    public function job()
    {
        return $this->belongsToMany('App\Models\Job', 'job_person', 'person_id', 'job_id');//->withPivot('qtd');
    }
}
