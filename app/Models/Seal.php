<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Seal
 * @package App\Models
 * @version December 3, 2020, 1:43 pm UTC
 *
 * @property \App\Models\Cargo $cargo
 * @property integer $cargo_id
 * @property string $seal
 */
class Seal extends Model
{
	use \Illuminate\Database\Eloquent\Factories\HasFactory;

    public $table = 'seals';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $with = ['cargo'];


    public $fillable = [
        'cargo_id',
        'seal'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'cargo_id' => 'integer',
        'seal' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'cargo_id' => 'required',
        'seal' => 'nullable|string|max:190',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function cargo()
    {
        return $this->belongsTo(\App\Models\Cargo::class, 'cargo_id');
    }
}
