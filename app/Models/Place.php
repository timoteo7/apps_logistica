<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Place
 * @package App\Models
 * @version November 26, 2020, 6:47 pm UTC
 *
 * @property \App\Models\City $city
 * @property string $name
 * @property integer $city_id
 */
class Place extends Model
{
	use HasFactory;

    public $table = 'places';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $with = ['city'];


    public $fillable = [
        'name',
        'city_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'city_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string|max:190',
        'city_id' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function city()
    {
        return $this->belongsTo(\App\Models\City::class, 'city_id');
    }
}
