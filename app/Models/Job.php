<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Job
 * @package App\Models
 * @version November 26, 2020, 8:15 pm UTC
 *
 * @property string $title
 * @property string $description
 */
class Job extends Model
{
	use HasFactory;

    public $table = 'jobs';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    //protected $with = ['person'];


    public $fillable = [
        'title',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required|string|max:190',
        'description' => 'nullable|string|max:190',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    public function person()
    {
        return $this->belongsToMany('App\Models\Person', 'job_person', 'person_id', 'job_id');//->withPivot('qtd');
    }

    
}
