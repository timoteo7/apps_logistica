<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Port
 * @package App\Models
 * @version December 4, 2020, 5:56 pm UTC
 *
 * @property \App\Models\City $city
 * @property boolean $isport
 * @property string $name
 * @property string $abbreviation
 * @property string $address
 * @property integer $city_id
 * @property string $phone
 * @property string $obs
 */
class Port extends Model
{
	use \Illuminate\Database\Eloquent\Factories\HasFactory;

    public $table = 'ports';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $with = ['city'];


    public $fillable = [
        'isport',
        'name',
        'abbreviation',
        'address',
        'city_id',
        'phone',
        'obs'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'isport' => 'boolean',
        'name' => 'string',
        'abbreviation' => 'string',
        'address' => 'string',
        'city_id' => 'integer',
        'phone' => 'string',
        'obs' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'isport' => 'required|boolean',
        'name' => 'required|string|max:190',
        'abbreviation' => 'nullable|string|max:50',
        'address' => 'nullable|string|max:190',
        'city_id' => 'nullable',
        'phone' => 'nullable|string|max:190',
        'obs' => 'nullable|string|max:190',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function city()
    {
        return $this->belongsTo(\App\Models\City::class, 'city_id');
    }
}
