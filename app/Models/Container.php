<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Container
 * @package App\Models
 * @version November 27, 2020, 3:04 pm UTC
 *
 * @property \App\Models\Person $person
 * @property \App\Models\Typesofcontainer $typeofcontainer
 * @property string $code
 * @property integer $person_id
 * @property integer $typeofcontainer_id
 * @property string $obs
 */
class Container extends Model
{
	use HasFactory;

    public $table = 'containers';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $with = ['person', 'typeofcontainer'];


    public $fillable = [
        'code',
        'person_id',
        'typeofcontainer_id',
        'obs'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'code' => 'string',
        'person_id' => 'integer',
        'typeofcontainer_id' => 'integer',
        'obs' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'code' => 'required|string|max:60',
        'person_id' => 'nullable',
        'typeofcontainer_id' => 'nullable',
        'obs' => 'nullable|string',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function person()
    {
        return $this->belongsTo(\App\Models\Person::class, 'person_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function typeofcontainer()
    {
        return $this->belongsTo(\App\Models\TypeOfContainer::class, 'typeofcontainer_id');
    }
}
