<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Route
 * @package App\Models
 * @version November 27, 2020, 5:34 pm UTC
 *
 * @property \App\Models\Person $consignee
 * @property \App\Models\Person $expedidor
 * @property \App\Models\Person $notify
 * @property \App\Models\Place $placedelivery
 * @property \App\Models\Place $placereceipt
 * @property string $customerreference
 * @property string|\Carbon\Carbon $receipttime
 * @property integer $expedidor_id
 * @property integer $consignee_id
 * @property integer $notify_id
 * @property integer $placereceipt_id
 * @property integer $placedelivery_id
 * @property string $status
 * @property string $situation
 */
class Route extends Model
{
	use \Illuminate\Database\Eloquent\Factories\HasFactory;

    public $table = 'routes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $with = ['expedidor', 'consignee', 'notify', 'placedelivery', 'placereceipt'];


    public $fillable = [
        'customerreference',
        'receipttime',
        'expedidor_id',
        'consignee_id',
        'notify_id',
        'placereceipt_id',
        'placedelivery_id',
        'status',
        'situation'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'customerreference' => 'string',
        'receipttime' => 'datetime',
        'expedidor_id' => 'integer',
        'consignee_id' => 'integer',
        'notify_id' => 'integer',
        'placereceipt_id' => 'integer',
        'placedelivery_id' => 'integer',
        'status' => 'string',
        'situation' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'customerreference' => 'required|string|max:190',
        'receipttime' => 'nullable',
        'expedidor_id' => 'nullable',
        'consignee_id' => 'nullable',
        'notify_id' => 'nullable',
        'placereceipt_id' => 'nullable',
        'placedelivery_id' => 'nullable',
        'status' => 'nullable|string|max:50',
        'situation' => 'nullable|string|max:50',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function consignee()
    {
        return $this->belongsTo(\App\Models\Person::class, 'consignee_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function expedidor()
    {
        return $this->belongsTo(\App\Models\Person::class, 'expedidor_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function notify()
    {
        return $this->belongsTo(\App\Models\Person::class, 'notify_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function placedelivery()
    {
        return $this->belongsTo(\App\Models\Place::class, 'placedelivery_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function placereceipt()
    {
        return $this->belongsTo(\App\Models\Place::class, 'placereceipt_id');
    }
}
