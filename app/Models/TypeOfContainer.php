<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class TypeOfContainer
 * @package App\Models
 * @version November 27, 2020, 12:36 pm UTC
 *
 * @property string $description
 * @property number $length
 * @property number $width
 * @property number $height
 * @property number $tare
 */
class TypeOfContainer extends Model
{
	use HasFactory;

    public $table = 'typesofcontainer';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'description',
        'length',
        'width',
        'height',
        'tare'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'description' => 'string',
        'length' => 'float',
        'width' => 'float',
        'height' => 'float',
        'tare' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'description' => 'required|string|max:190',
        'length' => 'nullable|numeric',
        'width' => 'nullable|numeric',
        'height' => 'nullable|numeric',
        'tare' => 'nullable|numeric',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    
}
