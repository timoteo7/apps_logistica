<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Country
 * @package App\Models
 * @version November 10, 2020, 12:03 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $cities
 * @property string $name
 * @property string $abbreviation
 * @property string $iso
 * @property string $flag_url
 */
class Country extends Model
{
    use HasFactory;

    public $table = 'countries';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'name',
        'abbreviation',
        'iso',
        'flag_url'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'abbreviation' => 'string',
        'iso' => 'string',
        'flag_url' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string|max:190',
        'abbreviation' => 'nullable|string|max:10',
        'iso' => 'nullable|string|max:20',
        'flag_url' => 'nullable|string|max:190',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function cities()
    {
        return $this->hasMany(\App\Models\City::class, 'country_id');
    }
}
