# 💻 Apps : Controle Logistico

Sistema para controle Logistica


## 🖇️ Clone

```
git clone https://gitlab.com/timoteo7/apps_logistica.git
```

## 🔧 Install

```sh
composer install
```

## 📋 Usage

```sh
php artisan serve
```

## Setup (Optional)

Se não quiser usar o Servidor web embutido do PHP e já tiver um servidor Apache (http://localhost/apps_logistica/)

```
sudo chmod -R 777 storage/framework storage/logs bootstrap/cache
sed -i "/APP_URL=http:\/\// s/localhost/localhost\/apps_logistica/" .env
npm i && npm run prod

```

Se quiser Banco de Dados Mysql

em seu arquivo `.env` defina os dados do teu Banco de Dados: 
DB_DATABASE, DB_USERNAME, DB_PASSWORD

sed -i "/DB_CONNECTION=/ s/sqlite/mysql/" .env
php artisan migrate:fresh --seed

## 🔑 Teste Autenticação

email:teste@teste.com

password:12345678


## 🔍 Tests
```
php vendor/bin/phpunit
```


## 📝 Routes
| Method        | URI           | Action                    |
|---------------|---------------|---------------------------|
| GET/HEAD      | login         | AuthController@login      |
| POST          | api/login     | AuthController@login      |
| POST          | api/contacts  | ContatoController@store   |

## 📊 Diagram
![Diagram](resources/diagram.png "Diagram" )

## ☑️ ToDo
- [x] ~~Validação de campos obrigatórios~~
- [x] ~~adicionar logo.png~~
- [x] ~~Campo do formulário com Select, puxar os Values~~
- [x] ~~first Render do Crud já trazer dados carregados na primeira listagem~~
- [ ] Atualizar a tabela de listagem do dataTable sem usar o index() quando altera por select
- [ ] Diagrama dos modelos de dados, relações entre tabelas, dos componentes js e captura de tela
- [ ] Tratar para verificar se .json existe
- [ ] validar formato de email e telefone recebido
- [ ] verificar tamanho do arquivo recebido (base64 ?) e extensão
- [ ] enviar email com as informações recebidas
- [ ] fazer testes automatizados

<details><summary markdown="span">

## 📜 Script History
</summary>

```
composer create-project laravel/laravel apps_logistica && cd apps_logistica
    #git config --global user.email "timoteoapolinario@gmail.com"
    #git config --global user.name "Timóteo"
git init && git add . && git commit -m 'Laravel'
git remote add origin https://gitlab.com/timoteo7/apps_logistica.git && git push -u origin master
wget https://raw.githubusercontent.com/timoteo7/files/master/.htaccess
```

```
composer require laravel/ui --dev
    php artisan ui vue --auth # ??? parece necessário para atualizar bootstrap.js e adicionar depedencias

composer require lucascudo/laravel-pt-br-localization        #???
php artisan vendor:publish --tag=laravel-pt-br-localization
sed -i "/'locale' => / s/en/pt-BR/" config/app.php
sed -i "/'fallback_locale' => / s/en/pt-br/" config/app.php
sed -i "/'faker_locale' => / s/en_US/pt_BR/" config/app.php
```

```
composer require jeroennoten/laravel-adminlte
php artisan adminlte:install --type=enhanced --interactive # sem o /public/vendor (basic assets)
php artisan adminlte:install --only=main_views
    #php artisan adminlte:install --with=auth_views, basic_views, basic_routes # ??? criar a view\layouts e atualizar home.php
    
npm i @fortawesome/fontawesome-free
npm i icheck-bootstrap
npm i overlayscrollbars
sed -i "/window.$ = window.jQuery = require('jquery');/a \\\n\t\trequire('overlayscrollbars');\n\t\trequire('../../vendor/almasaeed2010/adminlte/dist/js/adminlte');" resources/js/bootstrap.js
sed -i "/@import url('https:\/\/fonts.googleapis.com\/css?family=Nunito')/a \\\t@import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic');\n\t@import '~@fortawesome/fontawesome-free/css/all.css';\n\n\t@import '~overlayscrollbars/css/OverlayScrollbars.css';\n\n\t@import '~icheck-bootstrap/icheck-bootstrap.css';\n\n\t@import '../../vendor/almasaeed2010/adminlte/dist/css/adminlte.css';" resources/sass/app.scss
sed -i "s/\(@import '~bootstrap\/scss\/bootstrap';\)/\/\/\1/" resources/sass/app.scss
sed -i "/'enabled_laravel_mix' => / s/false/true/" config/adminlte.php
    #sed -i "/'use_ico_only' => / s/false/true/" config/adminlte.php

sed -i "/'asset_url'/a \\\n\t'mix_url' => env('MIX_ASSET_URL', null)," config/app.php
sed -i '/APP_URL=/a \\MIX_ASSET_URL="${ASSET_URL}"' .env
sed -i "/APP_URL=/a \\ASSET_URL=//" .env
sed -i "/.sass('resources\/sass\/app.scss', 'public\/css');/a \\\nmix.setResourceRoot(process.env.ASSET_URL);" webpack.mix.js
```


```
sed -i "/DB_CONNECTION=/ s/mysql/sqlite/" .env
sed -i "s/\(DB_DATABASE=\)/#\1/" .env
touch database/database.sqlite
    #git add --force database/database.sqlite
sudo chmod 777 database database/database.sqlite
php artisan migrate:fresh # --seed ???
```

```
php artisan make:controller UserController -mUser --api
sed -i "/});/a \\\n\tRoute::apiResources([\n\t\t'users'  =>'App\\\Http\\\Controllers\\\UserController',\n\t]);/" routes/api.php
cp resources/views/home.blade.php resources/views/crud.blade.php
cp resources/js/components/ExampleComponent.vue resources/js/components/Crud.vue
sed -i "/'example-component'/a Vue\.component('crud', require('\.\/components\/Crud.vue').default);" resources/js/app.js
```

```
npm install vue-tables-2
npm install vue-form-generator
    #npm install --save vue-nav-tabs
    #npm install vue-js-modal@1.3.35 --save
npm install --save laravel-vue2-validator ### ???
npm install --save @fortawesome/fontawesome-free icheck-bootstrap overlayscrollbars ### ???
npm run watch &
```

```
npm i --save @fortawesome/fontawesome-svg-core
npm i --save @fortawesome/free-solid-svg-icons
npm i --save @fortawesome/vue-fontawesome@2
npm install bootstrap-vue bootstrap vue-notification
```


```
composer require thedoctor0/laravel-factory-generator --dev
php artisan generate:model-factory          ## ModelFactory
sed -i "/this->call(UserSeeder::class);/a \\\t\tfactory(App\\\User::class, 4)->create();\n\t\tApp\\\User::firstOrCreate(['name' => 'Teste','email' => 'teste@teste.com'], ['password' => bcrypt('12345678'), 'updated_at' => date('Y-m-d H:i:s'), ]);" database/seeders/DatabaseSeeder.php
php artisan migrate:fresh --seed
    #php artisan db:seed --class=User
```

```
composer require infyomlabs/laravel-generator:8.0.x-dev doctrine/dbal --dev
php artisan vendor:publish --provider="InfyOm\Generator\InfyOmGeneratorServiceProvider"
sed -i "/'softDelete' => / s/true/false/" config/infyom/laravel_generator.php
sed -i "/'save_schema_file' =>/ s/true/false/" config/infyom/laravel_generator.php
```

```
composer require laracasts/generators --dev #pivot
```


```
         s="name:";                 s+="string(190), ";\
        s+="abbreviation:";         s+="string(10):nullable:unique, ";\
        s+="iso:";                  s+="string(20):nullable:unique, ";\
        s+="flag_url:";             s+="string(190):nullable ";\
model="Country";table="countries";bar="\\";
php artisan make:migration:schema create_${table}_table --schema="$s" --model=0
    php artisan migrate
php artisan infyom:model $model --fromTable --tableName=$table
    sed -i "/use Eloquent as Model;/a \\use Illuminate\\\Database\\\Eloquent\\\Factories\\\HasFactory;" app/Models/${model}.php
    perl -0777 -i -pe 's/(class Country extends Model\n\s*{)/$1\n\tuse HasFactory;/is' app/Models/${model}.php
php artisan generate:factory
sed -i "/User::factory(.*)->create();/a \\\t\t\\\App\\\Models$bar$bar$model::factory(10)->create();" database/seeders/DatabaseSeeder.php

php artisan db:seed
php artisan make:controller ${model}Controller -m$model --api
sed -i "/UserController'/a \\\t\t'$table'  =>'App\\\Http\\\Controllers\\$bar${model}Controller'," routes/api.php
```

```
         s="name:";                 s+="string(190), ";\
        s+="state:";                s+="string(190):nullable, ";\
        s+="country_id:";           s+="unsignedBigInteger:foreign:nullable ";\
model="City";table="cities";bar="\\";
php artisan make:migration:schema create_${table}_table --schema="$s" --model=0
    php artisan migrate
php artisan infyom:model $model --fromTable --tableName=$table
    sed -i "/use Eloquent as Model;/a \\use Illuminate\\\Database\\\Eloquent\\\Factories\\\HasFactory;" app/Models/${model}.php
    perl -0777 -i -pe 's/( extends Model\n\s*{)/$1\n\tuse HasFactory;/is' app/Models/${model}.php
php artisan generate:factory
sed -i "/User::factory(.*)->create();/a \\\t\t\\\App\\\Models$bar$bar$model::factory(10)->create();" database/seeders/DatabaseSeeder.php

php artisan db:seed
    #php artisan make:controller ${model}Controller -m$model --api
sed -i "/Route::apiResources(\[/a \\\t\t'$table' \t\t=>'App\\\Http\\\Controllers\\\CrudController'," routes/api.php
    sed -i "/ /a \\\t\t" routes/web.json
```

```
         s="name:";                 s+="string(190), ";\
        s+="city_id:";              s+="unsignedBigInteger:foreign:nullable ";\
model="Place";table="places";bar="\\";
php artisan make:migration:schema create_${table}_table --schema="$s" --model=0
    php artisan migrate
php artisan infyom:model $model --fromTable --tableName=$table
    sed -i "/use Eloquent as Model;/a \\use Illuminate\\\Database\\\Eloquent\\\Factories\\\HasFactory;" app/Models/${model}.php
    perl -0777 -i -pe 's/( extends Model\n\s*{)/$1\n\tuse HasFactory;/is' app/Models/${model}.php
php artisan generate:factory
sed -i "/User::factory(.*)->create();/a \\\t\t\\\App\\\Models$bar$bar$model::factory(10)->create();" database/seeders/DatabaseSeeder.php

php artisan db:seed
    #php artisan make:controller ${model}Controller -m$model --api
sed -i "/Route::apiResources(\[/a \\\t\t'$table' \t\t=>'App\\\Http\\\Controllers\\\CrudController'," routes/api.php
    sed -i "/ /a \\\t\t" routes/web.json
```

```
         s="name:";                 s+="string(190), ";\
        s+="address:";              s+="string(190):nullable, ";\
        s+="phone:";                s+="string(50):nullable, ";\
        s+="city_id:";              s+="unsignedBigInteger:foreign:nullable, ";\
        s+="obs:";                  s+="text:nullable ";\
model="Person";table="persons";bar="\\";
php artisan make:migration:schema create_${table}_table --schema="$s" --model=0
    php artisan migrate
php artisan infyom:model $model --fromTable --tableName=$table
    sed -i "/use Eloquent as Model;/a \\use Illuminate\\\Database\\\Eloquent\\\Factories\\\HasFactory;" app/Models/${model}.php
    perl -0777 -i -pe 's/( extends Model\n\s*{)/$1\n\tuse HasFactory;/is' app/Models/${model}.php
php artisan generate:factory
sed -i "/User::factory(.*)->create();/a \\\t\t\\\App\\\Models$bar$bar$model::factory(10)->create();" database/seeders/DatabaseSeeder.php

php artisan db:seed
    #php artisan make:controller ${model}Controller -m$model --api
sed -i "/Route::apiResources(\[/a \\\t\t'$table' \t\t=>'App\\\Http\\\Controllers\\\CrudController'," routes/api.php
    sed -i "/ /a \\\t\t" routes/web.json
```

```
         s="title:";                 s+="string(190), ";\
        s+="description:";          s+="string(190):nullable ";\
model="Job";table="jobs";bar="\\";
php artisan make:migration:schema create_${table}_table --schema="$s" --model=0
    php artisan migrate
php artisan infyom:model $model --fromTable --tableName=$table
    sed -i "/use Eloquent as Model;/a \\use Illuminate\\\Database\\\Eloquent\\\Factories\\\HasFactory;" app/Models/${model}.php
    perl -0777 -i -pe 's/( extends Model\n\s*{)/$1\n\tuse HasFactory;/is' app/Models/${model}.php
php artisan generate:factory
sed -i "/User::factory(.*)->create();/a \\\t\t\\\App\\\Models$bar$bar$model::factory(10)->create();" database/seeders/DatabaseSeeder.php

php artisan db:seed
    #php artisan make:controller ${model}Controller -m$model --api
sed -i "/Route::apiResources(\[/a \\\t\t'$table' \t\t=>'App\\\Http\\\Controllers\\\CrudController'," routes/api.php
    sed -i "/ /a \\\t\t" routes/web.json
```

```
php artisan make:migration:pivot    persons        jobs
php artisan migrate
```

```
         s="description:";          s+="string(190), ";\
        s+="length:";               s+="float:nullable, ";\
        s+="width:";                s+="float:nullable, ";\
        s+="height:";               s+="float:nullable, ";\
        s+="tare:";                 s+="float:nullable ";\
model="TypeOfContainer";table="typesofcontainer";bar="\\";
php artisan make:migration:schema create_${table}_table --schema="$s" --model=0
    php artisan migrate
php artisan infyom:model $model --fromTable --tableName=$table
    sed -i "/use Eloquent as Model;/a \\use Illuminate\\\Database\\\Eloquent\\\Factories\\\HasFactory;" app/Models/${model}.php
    perl -0777 -i -pe 's/( extends Model\n\s*{)/$1\n\tuse HasFactory;/is' app/Models/${model}.php
php artisan generate:factory
sed -i "/User::factory(.*)->create();/a \\\t\t\\\App\\\Models$bar$bar$model::factory(10)->create();" database/seeders/DatabaseSeeder.php

php artisan db:seed
    #php artisan make:controller ${model}Controller -m$model --api
sed -i "/Route::apiResources(\[/a \\\t\t'$table' \t\t=>'App\\\Http\\\Controllers\\\CrudController'," routes/api.php
    sed -i "/ /a \\\t\t" routes/web.json
```

```
         s="code:";                 s+="string(60), ";\
        s+="person_id:";            s+="unsignedBigInteger:foreign:nullable, ";\
        s+="typeofcontainer_id:";   s+="unsignedBigInteger:foreign:nullable, ";\
        s+="obs:";                  s+="text:nullable ";\
model="Container";table="containers";bar="\\";
php artisan make:migration:schema create_${table}_table --schema="$s" --model=0
    php artisan migrate
php artisan infyom:model $model --fromTable --tableName=$table
    sed -i "/use Eloquent as Model;/a \\use Illuminate\\\Database\\\Eloquent\\\Factories\\\HasFactory;" app/Models/${model}.php
    perl -0777 -i -pe 's/( extends Model\n\s*{)/$1\n\tuse HasFactory;/is' app/Models/${model}.php
php artisan generate:factory
sed -i "/User::factory(.*)->create();/a \\\t\t\\\App\\\Models$bar$bar$model::factory(10)->create();" database/seeders/DatabaseSeeder.php
php artisan db:seed

    #php artisan make:controller ${model}Controller -m$model --api
sed -i "/Route::apiResources(\[/a \\\t\t'$table' \t\t=>'App\\\Http\\\Controllers\\\CrudController'," routes/api.php
    sed -i "/ /a \\\t\t" routes/web.json
```

```
         s="customerreference:";    s+="string(190), ";\
        s+="receipttime:";          s+="dateTime:nullable, ";\
        s+="expedidor_id:";         s+="unsignedBigInteger:foreign:nullable, ";\
        s+="consignee_id:";         s+="unsignedBigInteger:foreign:nullable, ";\
        s+="notify_id:";            s+="unsignedBigInteger:foreign:nullable, ";\
        s+="placereceipt_id:";      s+="unsignedBigInteger:foreign:nullable, ";\
        s+="placedelivery_id:";     s+="unsignedBigInteger:foreign:nullable, ";\
        s+="status:";               s+="string(50):nullable, ";\
        s+="situation:";            s+="string(50):nullable ";\
model="Route";table="routes";bar="\\";
php artisan make:migration:schema create_${table}_table --schema="$s" --model=0
    php artisan migrate
php artisan infyom:model $model --fromTable --tableName=$table
    perl -0777 -i -pe 's/( extends Model\n\s*{)/$1\n\tuse \\Illuminate\\\Database\\Eloquent\\Factories\\HasFactory;/is' app/Models/${model}.php
php artisan generate:factory
sed -i "/User::factory(.*)->create();/a \\\t\t\\\App\\\Models$bar$bar$model::factory(10)->create();" database/seeders/DatabaseSeeder.php
php artisan db:seed

    #php artisan make:controller ${model}Controller -m$model --api
sed -i "/Route::apiResources(\[/a \\\t\t'$table' \t\t=>'App\\\Http\\\Controllers\\\CrudController'," routes/api.php
    sed -i "/ /a \\\t\t" routes/web.json
```

```
         s="route_id:";             s+="unsignedBigInteger:foreign:nullable, ";\
        s+="container_id:";         s+="unsignedBigInteger:foreign:nullable, ";\
        s+="packages:";             s+="unsignedInteger:nullable, ";\
        s+="weight:";               s+="float:nullable, ";\
        s+="volume:";               s+="float:nullable, ";\
        s+="amount:";               s+="float:nullable ";\
model="Cargo";table="cargos";bar="\\";
php artisan make:migration:schema create_${table}_table --schema="$s" --model=0
    php artisan migrate
php artisan infyom:model $model --fromTable --tableName=$table
    perl -0777 -i -pe 's/( extends Model\n\s*{)/$1\n\tuse \\Illuminate\\\Database\\Eloquent\\Factories\\HasFactory;/is' app/Models/${model}.php
php artisan generate:factory
sed -i "/User::factory(.*)->create();/a \\\t\t\\\App\\\Models$bar$bar$model::factory(10)->create();" database/seeders/DatabaseSeeder.php
php artisan db:seed

    #php artisan make:controller ${model}Controller -m$model --api
sed -i "/Route::apiResources(\[/a \\\t\t'$table' \t\t=>'App\\\Http\\\Controllers\\\CrudController'," routes/api.php
    sed -i "/ /a \\\t\t" routes/web.json
```


```
         s="cargo_id:";             s+="unsignedBigInteger:foreign, ";\
        s+="seal:";                 s+="string(190):nullable ";\
model="Seal";table="seals";bar="\\";
php artisan make:migration:schema create_${table}_table --schema="$s" --model=0
    php artisan migrate
php artisan infyom:model $model --fromTable --tableName=$table
    perl -0777 -i -pe 's/( extends Model\n\s*{)/$1\n\tuse \\Illuminate\\\Database\\Eloquent\\Factories\\HasFactory;/is' app/Models/${model}.php
php artisan generate:factory
sed -i "/User::factory(.*)->create();/a \\\t\t\\\App\\\Models$bar$bar$model::factory(10)->create();" database/seeders/DatabaseSeeder.php
php artisan db:seed

    #php artisan make:controller ${model}Controller -m$model --api
sed -i "/Route::apiResources(\[/a \\\t\t'$table' \t\t=>'App\\\Http\\\Controllers\\\CrudController'," routes/api.php
    sed -i "/ /a \\\t\t" routes/web.json
```

```
         s="isport:";               s+="boolean, ";\
        s+="name:";                 s+="string(190), ";\
        s+="abbreviation:";         s+="string(50):nullable, ";\
        s+="address:";              s+="string(190):nullable, ";\
        s+="city_id:";              s+="unsignedBigInteger:foreign:nullable, ";\
        s+="phone:";                s+="string(190):nullable, ";\
        s+="obs:";                  s+="string(190):nullable ";\
model="Port";table="ports";bar="\\";
php artisan make:migration:schema create_${table}_table --schema="$s" --model=0
    php artisan migrate
php artisan infyom:model $model --fromTable --tableName=$table
    perl -0777 -i -pe 's/( extends Model\n\s*{)/$1\n\tuse \\Illuminate\\\Database\\Eloquent\\Factories\\HasFactory;/is' app/Models/${model}.php
php artisan generate:factory
sed -i "/User::factory(.*)->create();/a \\\t\t\\\App\\\Models$bar$bar$model::factory(10)->create();" database/seeders/DatabaseSeeder.php
php artisan db:seed

    #php artisan make:controller ${model}Controller -m$model --api
sed -i "/Route::apiResources(\[/a \\\t\t'$table' \t\t=>'App\\\Http\\\Controllers\\\CrudController'," routes/api.php
    sed -i "/ /a \\\t\t" routes/web.json
```


```
         s="reference:";            s+="string(190), ";\
        s+="eta:";                  s+="dateTime:nullable, ";\
        s+="rta:";                  s+="dateTime:nullable, ";\
        s+="portloading_id:";       s+="unsignedBigInteger:foreign:nullable, ";\
        s+="portdischarge_id:";     s+="unsignedBigInteger:foreign:nullable, ";\
        s+="consignee_id:";         s+="unsignedBigInteger:foreign:nullable, ";\
        s+="vessel:";               s+="string(190):nullable, ";\
        s+="blnro:";                s+="string(100):nullable, ";\
        s+="bookingnro:";           s+="string(100):nullable, ";\
        s+="exportreference:";      s+="string(190):nullable, ";\
        s+="forwardingagent:";      s+="unsignedBigInteger:foreign:nullable, ";\
        s+="agentportdischarger:";  s+="unsignedBigInteger:foreign:nullable, ";\
        s+="document_url:";         s+="unsignedBigInteger:foreign:nullable, ";\
        s+="route_id:";             s+="unsignedBigInteger:foreign:nullable ";\
model="BillLanding";table="bills";bar="\\";
php artisan make:migration:schema create_${table}_table --schema="$s" --model=0
    php artisan migrate
php artisan infyom:model $model --fromTable --tableName=$table
    perl -0777 -i -pe 's/( extends Model\n\s*{)/$1\n\tuse \\Illuminate\\\Database\\Eloquent\\Factories\\HasFactory;/is' app/Models/${model}.php
php artisan generate:factory
sed -i "/User::factory(.*)->create();/a \\\t\t\\\App\\\Models$bar$bar$model::factory(10)->create();" database/seeders/DatabaseSeeder.php
php artisan db:seed

    #php artisan make:controller ${model}Controller -m$model --api
sed -i "/Route::apiResources(\[/a \\\t\t'$table' \t\t=>'App\\\Http\\\Controllers\\\CrudController'," routes/api.php
    sed -i "/ /a \\\t\t" routes/web.json
```

```
sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/apps_logistica.conf
sudo nano /etc/hosts   ????????
    #service network restart ????
sudo a2ensite apps_logistica && sudo systemctl reload apache2 && sudo apache2 -k restart
```

```
sed -i "/'asset_url'/a \\\n\t'mix_url' => env('MIX_ASSET_URL', null)," config/app.php
sed -i '/APP_URL=/a \\MIX_ASSET_URL="${ASSET_URL}"' .env
sed -i "/APP_URL=/a \\ASSET_URL=//" .env
sed -i "/.sass('resources\/sass\/app.scss', 'public\/css');/a \\\nmix.setResourceRoot(process.env.ASSET_URL);" webpack.mix.js
```



```
grep -Erins 'laravel-generator"?:\s?"?7' --exclude-dir={vendor,cache,node_modules} --include=*.{json,php,sh,txt,md} /var/www/html/
```



```
sed -i "/Support\\\ServiceProvider/a \\use Schema;" app/Providers/AppServiceProvider.php
sed -i "N;/public function boot()\n\s*{/a \\\t\tSchema::defaultStringLength(191);" app/Providers/AppServiceProvider.php
```
</details>

## 🏷️ Topics (tags)
##### #Logistica  #Paraguai

## ✍️ Author

👤 **[Timóteo](https://timoteo7.github.io/)**



## 🚀 Built With

* [Laravel](https://github.com/laravel/laravel) - A PHP framework for web artisans <p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="100"></p>
* [Laravel-AdminLTE](https://github.com/jeroennoten/Laravel-AdminLTE) - Easy AdminLTE integration with Laravel 5
* [InfyOm Laravel Generator](https://github.com/InfyOmLabs/laravel-generator) - API, Scaffold, Tests, CRUD
* vue-tables-2
* vue-form-generator
* vue-bootstrap-datetimepicker
* [laravel-excel](https://github.com/Maatwebsite/Laravel-Excel) - ????? Supercharged Excel exports and imports in Laravel ?????????


## ⚖️ License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

🛠️